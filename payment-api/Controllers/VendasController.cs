﻿using Microsoft.AspNetCore.Mvc;
using payment_api.Context;
using payment_api.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace payment_api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class VendasController : ControllerBase
    {
        public readonly DataBaseContext _context;
        public VendasController(DataBaseContext context)
        {
            _context = context;
        }

        // Obter todas as vendas
        [HttpGet]
        public IActionResult ObterTodos()
        {
            var vendas = _context.Vendas.ToList();
            return Ok(vendas);
        }

        // Obter uma venda pelo ID
        [HttpGet("{id}")]
        public IActionResult ObterPorID(int id)
        {
            var tarefa = _context.Vendas.Find(id);
            if (tarefa == null)
            {
                return NotFound();
            }
            return Ok(tarefa);
        }

        // Criar uma nova venda
        [HttpPost]
        public IActionResult CriarVenda(Venda venda)
        {
            if(venda.Pedidos.Count == 0 || venda.Status != "Aguardando Pagamento")
            {
                return BadRequest();
            }
            else
            {
               
               _context.Vendas.Add(venda);
               _context.SaveChanges();
               return Ok();
            }
        }

        // Atualizar uma venda
        [HttpPut("{id}")]
        public IActionResult Put(int id, Venda venda)
        {
            if(venda.Status != "Pagamento aprovado" || venda.Status != "Enviado para transportadora" || venda.Status != "Entregue" || venda.Status != "Cancelada" || venda.Pedidos.Count == 0)
            {
                return BadRequest();
            }
            else
            {
                var vendaAtual = _context.Vendas.Find(id);
                if (vendaAtual == null)
                {
                    return NotFound();
                }
                else
                {
                    if(vendaAtual.Status == "Aguardando Pagamento" && (venda.Status != "Pagamento aprovado" || venda.Status != "Cancelada"))
                    {
                        return BadRequest();
                    }
                    else if(vendaAtual.Status == "Pagamento aprovado" && (venda.Status != "Enviado para transportadora" || venda.Status != "Cancelada"))
                    {
                        return BadRequest();
                    }
                    else if(vendaAtual.Status == "Enviado para transportadora" && venda.Status != "Entregue")
                    {
                        return BadRequest();
                    }
                    else
                    {
                        vendaAtual.Status = venda.Status;
                        _context.Vendas.Update(vendaAtual);
                        _context.SaveChanges();
                        return Ok();
                    }
                }
            }
        }

        // Deletar uma venda
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
            {
                return NotFound();
            }
            else
            {
                _context.Vendas.Remove(venda);
                _context.SaveChanges();
                return Ok();
            }
        }
    }
}
