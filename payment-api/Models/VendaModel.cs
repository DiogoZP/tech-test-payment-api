namespace payment_api.Models
{
    public class Venda
    {
        public Vendedor Vendedor { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
        public List<Pedido> Pedidos { get; set; }
        
    }


public static class VendaEndpoints
{
	public static void MapVendaEndpoints (this IEndpointRouteBuilder routes)
    {
        routes.MapGet("/api/Venda", () =>
        {
            return new [] { new Venda() };
        })
        .WithName("GetAllVendas");

        routes.MapGet("/api/Venda/{id}", (int id) =>
        {
            //return new Venda { ID = id };
        })
        .WithName("GetVendaById");

        routes.MapPut("/api/Venda/{id}", (int id, Venda input) =>
        {
            return Results.NoContent();
        })
        .WithName("UpdateVenda");

        routes.MapPost("/api/Venda/", (Venda model) =>
        {
            //return Results.Created($"/Vendas/{model.ID}", model);
        })
        .WithName("CreateVenda");

        routes.MapDelete("/api/Venda/{id}", (int id) =>
        {
            //return Results.Ok(new Venda { ID = id });
        })
        .WithName("DeleteVenda");  
    }
}}