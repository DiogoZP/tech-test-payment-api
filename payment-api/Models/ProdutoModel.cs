﻿namespace payment_api.Models
{
    public class Produto
    {
        public string Nome { get; set; }
        public double Preco { get; set; }
    }
}
